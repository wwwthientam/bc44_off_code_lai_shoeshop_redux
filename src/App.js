import logo from "./logo.svg";
import "./App.css";
import Ex_ShoeShop_Redux from "./EX_ShoeShop_Redux/Ex_ShoeShop_Redux";

function App() {
  return (
    <div>
      <Ex_ShoeShop_Redux />
    </div>
  );
}

export default App;
