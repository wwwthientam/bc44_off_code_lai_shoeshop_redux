import { BUY_SHOE, VIEW_DETAIL } from "../constant/shoeConstant";

export let viewDetailAction = (shoe) => {
  return {
    type: VIEW_DETAIL,
    payload: shoe,
  };
};
export let handleBuy = (shoe) => {
  return {
    type: BUY_SHOE,
    payload: shoe,
  };
};
