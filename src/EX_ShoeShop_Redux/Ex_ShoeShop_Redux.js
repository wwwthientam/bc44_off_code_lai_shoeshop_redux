import React, { Component } from "react";
import { ShoeArr } from "./Data";
import ListShoe from "./ListShoe";
import Detail from "./Detail";
import CartShoe from "./CartShoe";
export default class Ex_ShoeShop_Redux extends Component {
  render() {
    return (
      <div>
        <h1 className="text-center text-light bg-success">
          Exercise Shoe Shop Redux
        </h1>
        <Detail />
        <div className="row">
          <CartShoe />
          <ListShoe />
        </div>
      </div>
    );
  }
}
